# Final Fantasy XIV in D&D 5E - Crystalline Quests Edition 

## Acknowledgement
This project is inspired FFXIV x D&D Compendium by [/u/SilentSoren](https://www.reddit.com/u/SilentSoren) from reddit community r/FFXIVxDnD. Thank you for the great work for marrying these two games we love! You can find the latest document at [GMBinder](https://www.gmbinder.com/share/-LsDqsNbupzeLhkTIcPv). Please support them at r/FFXIVxDnD or their twitch https://www.twitch.tv/sorenofasgard and YouTube [Soren of Asgard Ch.](https://www.youtube.com/channel/UCdpp8LHAQjOGHbrePHuazpw). 
Also thank Lxran#2292 and craigtrevor#1826 for converting FFXIV 5E Compendium on 5eTools, which served as the basis of the project. Thank you folks for the hard work! 
I also want to take the time to appreciate my partners, my friends, and my game group for supporting me to recreate my interpretation of our beloved XIVCQ universe. Thank you and hopefully, you enjoy the experience!

## What's the Deal?
Flavorful and powerful nine races, Hydaelyn-specific backgrounds, a very wonky but multi-class friendly prestige class system to capture the classic FFXIV classes and jobs! There are also plans for more spells and monsters, a compendium of FFXIV lore, as well as a selection of Feats (different variations of Echoes!) rooted in the lore of Final Fantasy XIV. Inspired by FFXIV x D&D Compendium by SilentSoren, This project aims to provide more flavor and fluff for your Eorzean adventures in a D&D game!
This book is written specifically to be used with 5eTools - so you can load this book and play as a Warrior of Light in your favorite VTT. However, keep in mind it's not balanced for traditional DnD 5E play or even within the system! If you would like to make it balanced, feel free to leave any comments at this [git repository](https://gitlab.com/crystalinequest/FFXIV5E).

## Usage
This repository is intended to be use with 5eTools. Please do not share this repository or links to 5eTools in public forum such as Twitter, Facebook, Reddit, Roll20 Community, D&D Beyond, Twitch, etc.  

To use this Homebrew, get a raw link to the Json file located in the root of the repo: [link]
On 5eTools, go to Utilities > Homebrew Manager > Load from URL, and paste in the URL to the raw JSON file. You are good to view the contents!

## Support & Contribution
Please open an issue here or contact me on Discord at Esurielt#1111 through 5eTools server.  

## Roadmap
Race Overhaul - Started 02/29/2022
Class Overhaul
Backgrounds of Hydaelyn
History of Hydaelyn
Crystalline Feats
DM Notes

## Project status
Active